Source: fastdnaml
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Charles Plessy <plessy@debian.org>,
           Étienne Mollier <emollier@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.7.2
Vcs-Browser: https://salsa.debian.org/med-team/fastdnaml
Vcs-Git: https://salsa.debian.org/med-team/fastdnaml.git
Homepage: https://www.life.illinois.edu/gary/programs/fastDNAml.html
Rules-Requires-Root: no

Package: fastdnaml
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Suggests: phylip
Description: Tool for construction of phylogenetic trees of DNA sequences
 fastDNAml is a program derived from Joseph Felsenstein's version 3.3 DNAML
 (part of his PHYLIP package).  Users should consult the documentation for
 DNAML before using this program.
 .
 fastDNAml is an attempt to solve the same problem as DNAML, but to do so
 faster and using less memory, so that larger trees and/or more bootstrap
 replicates become tractable.  Much of fastDNAml is merely a recoding of the
 PHYLIP 3.3 DNAML program from PASCAL to C.
 .
 Note that the homepage of this program is not available any more and so
 this program will probably not see any further updates.
